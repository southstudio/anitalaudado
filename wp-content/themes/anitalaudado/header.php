
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo get_bloginfo( 'name' );?>">
        <title><?php bloginfo('name'); ?><?php if (!is_front_page()) {?> | <?php wp_title(''); }?></title>
        <?php wp_head();?>
    </head>
    <body <?php body_class(); ?>>
        <div id="top"></div>
        
        <header>
            <!-- MENU CONTAINER -->
            <div class="menu-container">
                <div class="container">
                    <nav class="navbar navbar-expand-md">
                        <a class="site-logo primary-text fs-15" href="<?php echo esc_url( home_url( '/' )); ?>">
                            <?php if ( get_theme_mod( 'site_logo' ) ): ?>
                                <img class="img-fluid" src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            <?php else :
                                esc_url(bloginfo('name'));
                            endif; ?>
                        </a>
                        <button class="navbar-toggler collapsed" type="button">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>

                        <!-- MENU -->
                        <?php
                            wp_nav_menu(array(
                                'theme_location'  => 'primary',
                                'container'       => 'div',
                                'container_id'    => 'navbarTop',
                                'container_class' => 'collapse navbar-collapse',
                                'menu_class'      => 'navbar-nav ml-auto',
                                'add_li_class'    => 'nav-item',
                                'add_a_class'     => 'fs-15 primary-text'
                            ));
                        ?><!-- END MENU -->
                    </nav>
                </div>
            </div><!-- END MENU CONTAINER -->
        </header>

        <div id="mainContent" class="main-content">