<?php get_header(); ?>

    <!-- COVER -->
    <div class="page-cover archive-cover d-flex flex-column">
        <div class="container mt-auto">
            <div class="row mb-10 mb-sm-30">
                <div class="col-12 d-flex flex-column">
                    <h1 class="fs-70 bold-font mxy-0 mt-auto" data-aos="fade-up" data-aos-delay="400">Explore some of my <br />finest work.</h1>
                </div>
            </div>
        </div>

        <div class="container tags-container">
            <div class="row">
                <div class="col-12 col-lg-7 d-flex flex-column">
                    <?php
                        //$tags = get_the_terms( $post->ID, 'work_tags');
                        $tags = get_terms( array(
                            'taxonomy' => 'work_tags',
                        ) );
                        if ($tags && ! is_wp_error($tags)): ?>
                            <div class="work-tags d-flex align-items-center flex-wrap">
                                <?php foreach($tags as $tag): ?>
                                    <a class="primary-text fs-30" href="<?php echo get_term_link( $tag->slug, 'work_tags'); ?>">#<?php echo $tag->name; ?></a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif;
                    ?>
                </div>
            </div>
        </div>
    </div><!-- END COVER -->

    <!-- POSTS -->
    <?php
        if ( have_posts() ) : ?>
            <div id="site" class="container">
                <div class="row">
                    <?php
                        while ( have_posts() ) : the_post();
                            get_template_part('template-parts/grid-work', get_post_format());
                        endwhile;
                    ?>
                </div>
            </div>
        <?php else : ?>
            <div id="site" class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="fs-30 mxy-0">No results for now.</h1>
                    </div>
                </div>
            </div>
        <?php endif;
    ?><!-- END POSTS -->

<?php get_footer(); ?>