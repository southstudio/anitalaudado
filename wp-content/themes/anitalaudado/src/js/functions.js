// Add class
export const addClass = (el, cl) => {
    el.classList.add(cl);
}

// Remove class
export const removeClass = (el, cl) => {
    el.classList.remove(cl);
}

// Toggle class
export const toggleClass = (el, cl) => {
    el.classList.toggle(cl);
}

// Fade in function
export const fadeIn = el => {
    addClass(el, 'show');
    removeClass(el, 'hide');
}

// Fade out function
export const fadeOut = el => {
    addClass(el, 'hide');
    removeClass(el, 'show');
}

// Responsive menu
export const responsiveMenu = () => {
    var toggler = document.querySelector('.navbar-toggler');
    var windowWidth = window.innerWidth;

    if (windowWidth < 768) {
        toggler.onclick = () => {
            toggleClass(document.body, 'menu-open');
            return false;
        }
    }
    else{
        removeClass(document.body, 'menu-open');
    }
};

// Lazy load images on viewport
export const lazyLoadInViewport = () => {
    let images = document.querySelectorAll('.lazyload');
    let placeholder = document.querySelectorAll('.placeholder');
    images.forEach(img => {
        var bounding = img.getBoundingClientRect();
        if (bounding.top <= window.innerHeight - 200) {
            fadeIn(img);
        }
    });
    placeholder.forEach(ph => {
        var bounding = ph.getBoundingClientRect();
        if (bounding.top <= window.innerHeight - 200) {
            fadeOut(ph);
        }
    });
}

export const lazyLoadContainer = () => {
    let container = document.querySelectorAll('.lazyload-container');
    container.forEach(con => {
        var image = con.querySelector('.lazyload');
        image.onload = () => {
            con.style.height = image.offsetHeight + 'px';
        };
    });
}

export const resizedLazyLoadContainer = () => {
    let container = document.querySelectorAll('.lazyload-container');
    container.forEach(con => {
        var image = con.querySelector('.lazyload');
        con.style.height = image.offsetHeight + 'px';
    });
}

const anchorLinkHandler = e => {
    e.preventDefault();
    const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
    const targetAnchor = document.querySelector('#top');
    if (!targetAnchor) return;
    const originalTop = distanceToTop(targetAnchor);

    window.scrollBy({
        top: originalTop,
        left: 0,
        behavior: "smooth"
    });
    
    const checkIfDone = setInterval(() => {
        if (distanceToTop(targetAnchor) === 0) {
            targetAnchor.tabIndex = "-1";
            targetAnchor.focus();
            window.history.pushState("", "", '');
            clearInterval(checkIfDone);
        }
    }, 100);
    
}

export const goToAnchor = () => {
    jQuery('.page-scroll').click(function() {
        var anchor = jQuery(this);
        jQuery('html, body').animate({ scrollTop: (jQuery(anchor.attr('href')).offset().top - 80) }, 'slow');
        return false;
    });
}

export const stripContentImages = () => {
    var el = document.querySelectorAll('.post-content p');
    el.forEach(elm => {
        var parent = elm.parentNode;
        var images = elm.querySelectorAll('img');
        images.forEach(img => {
            parent.insertBefore(img, elm);
            parent.removeChild(elm);
        })
    });
}