import 'bootstrap';
import AOS from 'aos';
import LazyLoad from 'lazyload';
import { responsiveMenu, lazyLoadInViewport, lazyLoadContainer, resizedLazyLoadContainer, goToAnchor,
        stripContentImages } from './functions';

jQuery(document).ready($ => {
    // Animate on scroll
    AOS.init();

    let images = document.querySelectorAll(".lazyload");
    new LazyLoad(images);
    
    responsiveMenu();

    lazyLoadInViewport();

    lazyLoadContainer();

    goToAnchor();

    stripContentImages();

    // On scroll functions
    window.onscroll = () => { lazyLoadInViewport(); }

    // On resize functions
    window.onresize = () => {
        responsiveMenu();
        lazyLoadContainer();
        resizedLazyLoadContainer();
    };
});