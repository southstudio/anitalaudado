<?php get_header();?>
    <!-- COVER -->
    <div class="page-cover home-cover d-flex flex-column">
        <div class="cover-container container mt-auto">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex align-items-end justify-content-between">
                        <h1 class="fs-70 bold-font mxy-0" data-aos="fade-up" data-aos-delay="400">Hi, my name is <a href="<?php bloginfo('url'); ?>/about">Ana Laudado</a> <br>and I’m a <a href="<?php bloginfo('url'); ?>/work">UI Designer</a> based <br>in <a href="<?php bloginfo('url'); ?>/contact">Copenhagen</a>.</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="mxy-0">Please keep scrolling,</p>
                    <p class="mxy-0 mb-30">I want to show you some something.</p>
                    <a class="page-scroll go-down" href="#site">
                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow-left.svg" alt="Anita Laudado">
                    </a>
                </div>
            </div>
        </div>
    </div><!-- END COVER -->
    
    <div id="site" class="site mt-50">
        <?php
            $args                               = array( 'post_type' => 'al_work' );
            $query_work                         = new WP_Query($args);
        ?>

        <!-- ABOVE BANNER PROJECTS -->
        <div class="container">
            <div class="row">
                <?php
                    if ( $query_work->have_posts() )    : 
                    while ( $query_work->have_posts() ) : $query_work->the_post();
                    $position                           = get_field( 'project_order' );
                    $description                        = get_field( 'short_description');
                    $aboveWorkThumbnail                 = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    if ($position == 'Above') :
                ?>
                    <div class="col-12 proj-container mb-100">
                        <div class="row d-block d-lg-flex align-items-stretch h-100">
                            <div class="col-12 col-lg-7 proj-col proj-col--img mb-20 mb-lg-0">
                                <div onclick="location.href='<?php echo the_permalink(); ?>';" class="h-100 proj-holder">
                                    <div class="proj-img h-100 lazyload hide"
                                         style="background: url('<?php bloginfo('template_url'); ?>/dist/images/placeholder.png') center center/cover no-repeat"
                                         data-src="<?php echo $aboveWorkThumbnail[0]; ?>">
                                    </div>
                                    <div class="h-100 show placeholder"
                                         style="background: url('<?php bloginfo('template_url'); ?>/dist/images/placeholder.png') center center/cover no-repeat">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-5 proj-col proj-col--info">
                                <div class="proj-info d-lg-flex align-self-stretch flex-column">
                                    <div onclick="location.href='<?php echo the_permalink(); ?>';" class="mb-50 mb-lg-auto" style="cursor: pointer;">
                                        <h1 class="fs-30 mxy-0 mb-20">
                                            <?php the_title(); ?>
                                        </h1>
                                        <p class="fs-15 mxy-0">
                                            <?php echo $description ?>
                                        </p>
                                    </div>
                                    <a class="primary-text fs-15 proj-link d-flex align-items-center" href="<?php echo the_permalink(); ?>">
                                        View more
                                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endif;
                    endwhile;
                    endif;
                ?>
            </div>
        </div><!-- END ABOVE BANNER PROJECTS -->

        <!-- HOME BANNER -->
        <div class="section-banner">
            <div class="container">
                <div class="row d-block d-lg-flex align-items-center">
                    <div class="col-12 col-lg-7 mb-20 mb-lg-0">
                        <h1 class="fs-30 mxy-0">Always trying to reach that balance between usability and aesthetics in the most efficient way possible.</h1>
                    </div>
                    <div class="col-12 col-lg-5">
                        <a class="primary-text fs-15 d-flex align-items-center" href="about">
                            About me
                            <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- END HOME BANNER -->

        <!-- BENEATH BANNER PROJECTS -->
        <div class="container">
            <div class="row">
                <?php
                    if ( $query_work->have_posts() )    : 
                    while ( $query_work->have_posts() ) : $query_work->the_post();
                    $positionBeneath                    = get_field( 'project_order' );
                    $descriptionBeneath                 = get_field( 'short_description');
                    $beneathWorkThumbnail               = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                    if ($positionBeneath == 'Beneath') :
                ?>
                    <div class="col-12 proj-container mb-100">
                        <div class="row d-block d-lg-flex align-items-stretch h-100">
                            <div class="col-12 col-lg-7 proj-col proj-col--img mb-20 mb-lg-0">
                                <div onclick="location.href='<?php echo the_permalink(); ?>';" class="h-100 proj-holder">
                                    <div class="proj-img h-100 lazyload hide"
                                         style="background: url('<?php bloginfo('template_url'); ?>/dist/images/placeholder.png') center center/cover no-repeat"
                                         data-src="<?php echo $beneathWorkThumbnail[0]; ?>">
                                    </div>
                                    <div class="h-100 show placeholder"
                                         style="background: url('<?php bloginfo('template_url'); ?>/dist/images/placeholder.png') center center/cover no-repeat">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-5 proj-col proj-col--info">
                                <div class="proj-info d-lg-flex align-self-stretch flex-column">
                                    <div onclick="location.href='<?php echo the_permalink(); ?>';" class="mb-50 mb-lg-auto" style="cursor: pointer;">
                                        <h1 class="fs-30 mxy-0 mb-20">
                                            <?php the_title(); ?>
                                        </h1>
                                        <p class="fs-15 mxy-0">
                                            <?php echo $descriptionBeneath ?>
                                        </p>
                                    </div>
                                    <a class="primary-text fs-15 proj-link d-flex align-items-center" href="<?php echo the_permalink(); ?>">
                                        View more
                                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    endif;
                    endwhile;
                    endif;
                ?>
            </div>
        </div><!-- END BENEATH BANNER PROJECTS -->

        <?php wp_reset_postdata(); ?>

    </div>
<?php get_footer();?>