<?php
    // Template Name: Contact Page
    get_header();
?>
    <!-- COVER -->
    <div class="page-cover d-flex">
        <div class="container mt-auto">
            <div class="row mb-10 mb-sm-30">
                <div class="col-12 col-lg-7 d-flex flex-column">
                    <h1 class="fs-70 bold-font mxy-0 mt-auto" data-aos="fade-up" data-aos-delay="400">Thanks for stopping by! <br />Now let’s keep in touch.</h1>
                </div>
            </div>
        </div>
    </div><!-- END COVER -->

    <div id="site" class="contact-page">
        <!-- CONTACT BANNER -->
        <div class="section-banner mb-50">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-5 offset-lg-7">
                        <p class="fs-30 mb-50 mxy-0 mb-lg-0">If you like what I do and you think we can work together, drop me a line or call me and share with me your exciting project. I would love to hear from you!</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-7">
                        <?php if ( get_theme_mod( 'email' ) ): ?>
                            <a class="fs-50 primary-text" href="mailto:<?php echo get_theme_mod( 'email' ); ?>">
                                <?php echo get_theme_mod( 'email' ); ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( get_theme_mod( 'tel' ) ): ?>
                            <p class="fs-50 primary-text mxy-0">
                                <?php echo get_theme_mod( 'tel' ); ?>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div><!-- END CONTACT BANNER -->

        <div class="container">

            <div class="row">
                <div class="col-12 col-lg-5 offset-lg-7">
                    <a class="primary-text fs-15 d-flex align-items-center" href="<?php bloginfo('url'); ?>/work">
                        My work
                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                    </a>
                    <div class="mt-50"></div>
                    <a class="primary-text fs-15 d-flex align-items-center" href="<?php bloginfo('url'); ?>/about">
                        About me
                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                    </a>
                </div>
            </div>
        </div>
    </div><!-- END CONTACT PAGE -->
<?php get_footer(); ?>