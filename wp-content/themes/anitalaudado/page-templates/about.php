<?php
    // Template Name: About Page
    get_header();
?>
    <!-- COVER -->
    <div class="page-cover d-flex">
        <div class="container mt-auto">
            <div class="row mb-10 mb-sm-30">
                <div class="col-12 col-lg-7 d-flex flex-column">
                    <h1 class="fs-70 bold-font mxy-0 mt-auto" data-aos="fade-up" data-aos-delay="400">Hello there!</h1>
                    <h1 class="fs-70 bold-font mxy-0" data-aos="fade-up" data-aos-delay="400">I’m Ana :)</h1>
                </div>
            </div>
        </div>
    </div><!-- END COVER -->

    <div id="site" class="about-page">
        <!-- ABOUT BANNER -->
        <div class="section-banner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-5 offset-lg-7">
                        <h1 class="fs-50 mxy-0 mb-50 mb-lg-0">Specialized in User Interface & Branding.</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-7">
                        <h1 class="fs-30 mb-50 mb-lg-0 mxy-lg-0">I’m a UI Designer, trying to reach that balance between usability and aesthetics in the most efficient way possible.</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-5 offset-lg-7">
                        <p class="fs-15 mxy-0 mb-20">I’ve been a designer for 7+ years. I have a BFA in Graphic Design degree from Universidad Siglo 21, where I’ve been an assistant professor for 2 years.</p>
                        <p class="fs-15 mxy-0 mb-20">Over the time I leaned towards digital design, discovering my passion for User Interface and iconography. I made an UI/UX Design Specialization by CALARTS (California Institute of the Arts), and now I’m currently doing a Product Designer course (by Aerolab) to keep me on date.</p>
                        <p class="fs-15 mxy-0">I have worked in some of the most recognized design studios and advertising agencies in Córdoba, working on projects from Greenpeace to the Córdoba Province Government. I also co-founded <a class="secondary-text" href="http://south.studio/" target="_blank">South Studio</a>.</p>
                    </div>
                </div>
            </div>
        </div><!-- END ABOUT BANNER -->

        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <h1 class="fs-30 mb-50 mb-lg-0 mxy-lg-0">I'm from Ushuaia, in the cold and remote island of Tierra del Fuego (Argentina).</h1>
                </div>

                <div class="col-12 col-lg-5 offset-lg-7">
                    <p class="mb-50">I moved to a bigger city at a young age to pursue my dream of becoming a Graphic Designer (spoiler alert: I did it!).  Always dreaming big, I wanted to live abroad and continue my adventure, and that’s exactly what I’m doing here in Copenhagen.</p>

                    <a class="primary-text fs-15 d-flex align-items-center" href="<?php bloginfo('url'); ?>/work">
                        My work
                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                    </a>
                </div>
            </div>
        </div>

        <div class="container mt-130">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <div class="lazyload-container">
                        <img class="w-100 show placeholder" src="<?php  bloginfo('template_url'); ?>/dist/images/placeholder.png" alt="Anita Laudado">
                        <img class="w-100 hide lazyload" data-src="<?php bloginfo('template_url'); ?>/dist/images/pupa2.png" alt="Anita Laudado">
                    </div>
                </div>
                <div class="col-12 col-lg-5 d-flex flex-column align-self-stretch">
                    <div class="interests mt-auto mb-auto">
                        <p class="fs-15 uppercase mt-50 mt-lg-0 mxy-0">Music</p>
                        <p class="fs-15 uppercase mxy-0">Cooking</p>
                        <p class="fs-15 uppercase mxy-0">Reading</p>
                        <p class="fs-15 uppercase mxy-0">Movies & Series</p>
                        <p class="fs-15 uppercase mb-50 mb-lg-0 mxy-0">Animals & Environment</p>
                    </div>
                    <a class="primary-text fs-15 d-flex align-items-center" href="<?php bloginfo('template_url'); ?>/dist/docs/Ana_Laudado.pdf" download="Ana Laudado">
                        Download my CV
                        <img class="img-fluid" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                    </a>
                </div>
            </div>
        </div>

    </div><!-- END ABOUT PAGE -->
<?php get_footer(); ?>