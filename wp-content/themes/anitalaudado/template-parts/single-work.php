<?php
    $header_image  = get_field( 'header_image' );
    $description   = get_field( 'short_description');
    $workThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>

<!-- COVER -->
<?php
    if (!empty($header_image)) : ?>
        <div class="page-cover proj-cover" style="background: url('<?php echo $header_image ?>') center center/cover no-repeat;"></div>
    <?php else : ?>
        <div class="page-cover proj-cover" style="background: url('<?php echo $workThumbnail[0]; ?>') center center/cover no-repeat;"></div>
    <?php endif;
?>

<div class="container mt-100">
    <div class="row">
        <div class="col-12 col-lg-7">
            <h1 class="fs-50 mxy-0"><?php the_title(); ?></h1>
        </div>

        <div class="col-12 col-lg-5 d-lg-flex align-items-center justify-content-end">
            <a class="primary-text fs-15 proj-link d-flex align-items-center reverse" href="<?php bloginfo('url'); ?>/work">
                <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow-left.svg" alt="Anita Laudado">
                All work
            </a>
        </div>
    </div>

    <div class="row mt-50">
        <div class="col-12 col-lg-7">
            <p class="fs-30 mxy-0"><?php echo $description; ?></p>
        </div>
    </div>

    <div class="row post-tags">
        <div class="col-12">
            <?php
                $tags = get_the_terms( $post->ID, 'work_tags');
                if ($tags && ! is_wp_error($tags)): ?>
                    <div class="work-tags d-flex align-items-center flex-wrap">
                        <?php foreach($tags as $tag): ?>
                            <a class="primary-text" href="<?php echo get_term_link( $tag->slug, 'work_tags'); ?>">#<?php echo $tag->name; ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif;
            ?>
        </div>
    </div>
</div>

<div class="container post-content mt-100">
    <?php the_content(); ?>
</div>

<div class="container mt-50">
    <a class="primary-text fs-15 proj-link d-flex align-items-center reverse" href="<?php bloginfo('url'); ?>/work">
        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow-left.svg" alt="Anita Laudado">
        All work
    </a>
</div>