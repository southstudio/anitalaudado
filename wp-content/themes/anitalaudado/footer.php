            <footer>
                <div class="container">
                    <div class="footer-container d-flex align-items-stretch">
                        <div class="row align-items-stretch flex-fill">
                            <div class="col-12 col-md-4">
                                <div class="column-footer d-flex flex-column">
                                    <div class="mb-auto d-md-none">
                                        <?php if ( get_theme_mod( 'tel' ) ): ?>
                                            <p class="fs-15 primary-text mxy-0 mt-auto" href="mailto:<?php echo get_theme_mod( 'tel' ); ?>">
                                                <?php echo get_theme_mod( 'tel' ); ?>
                                            </p>
                                        <?php endif; ?>
                                        <?php if ( get_theme_mod( 'email' ) ): ?>
                                            <a class="fs-15 primary-text" href="mailto:<?php echo get_theme_mod( 'email' ); ?>">
                                                <?php echo get_theme_mod( 'email' ); ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <!-- FOOTER MENU -->
                                    <?php
                                        wp_nav_menu(array(
                                            'theme_location'  => 'footer',
                                            'menu_class'      => 'menu-footer',
                                            'add_li_class'    => 'nav-item',
                                            'add_a_class'     => 'fs-15 primary-text uppercase'
                                        ));
                                    ?><!-- END MENU -->
                                    <?php if ( get_theme_mod( 'email' ) ): ?>
                                        <div class="mt-auto d-none d-md-block">
                                            <a class="fs-15 primary-text" href="mailto:<?php echo get_theme_mod( 'email' ); ?>">
                                                <?php echo get_theme_mod( 'email' ); ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="column-footer d-flex flex-column">
                                    <!-- SOCIAL MEDIA -->
                                    <?php if ( get_theme_mod( 'behance' ) ): ?>
                                        <a class="fs-15 primary-text uppercase mt-auto mt-md-0" href="<?php echo get_theme_mod( 'behance' ); ?>" target="_blank">
                                            Behance
                                        </a>
                                    <?php endif; ?>
                                    <?php if ( get_theme_mod( 'linkedin' ) ): ?>
                                        <a class="fs-15 primary-text uppercase" href="<?php echo get_theme_mod( 'linkedin' ); ?>" target="_blank">
                                            Linkedin
                                        </a>
                                    <?php endif; ?>
                                    <?php if ( get_theme_mod( 'instagram' ) ): ?>
                                        <a class="fs-15 primary-text uppercase" href="<?php echo get_theme_mod( 'instagram' ); ?>" target="_blank">
                                            Instagram
                                        </a>
                                    <?php endif; ?>
                                    <?php if ( get_theme_mod( 'tel' ) ): ?>
                                        <p class="fs-15 primary-text mxy-0 mt-auto d-none d-md-block" href="mailto:<?php echo get_theme_mod( 'tel' ); ?>">
                                            <?php echo get_theme_mod( 'tel' ); ?>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="column-footer d-flex flex-md-column align-items-center justify-content-between">
                                    <a class="page-scroll go-top order-2 order-md-1 ml-lg-auto mt-auto mt-md-0" href="#top">
                                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow-left.svg" alt="Anita Laudado">
                                    </a>
                                    <p class="primary-text order-1 order-md-2 fs-15 mxy-0 mt-auto ml-md-auto">
                                        Made with <img class="heart" src="<?php bloginfo('template_url'); ?>/dist/images/heart.svg" alt="Anita Laudado" /> by <a class="primary-text" href="http://south.studio/" target="_blank">South Studio</a>.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div><!-- /.main-content -->
        
        <?php wp_footer(); ?> 
    </body>
</html>