<?php
    // Logo
    function site_logo( $wp_customize ){
        // Settings
        $wp_customize->add_setting( 'site_logo' );

        // Controls
        // Logo
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'site_logo',
                array(
                    'label'      => 'Upload a logo',
                    'section'    => 'title_tagline',
                    'settings'   => 'site_logo'
                )
            )
        );
    }
    add_action('customize_register', 'site_logo');

    // Contact info
    function contact_info( $wp_customize ){
        // Settings
        $wp_customize->add_setting( 'tel', array( 'default' => '' ) );
        $wp_customize->add_setting( 'email', array( 'default' => '' ) );
        $wp_customize->add_setting( 'behance', array( 'default' => '' ) );
        $wp_customize->add_setting( 'linkedin', array( 'default' => '' ) );
        $wp_customize->add_setting( 'instagram', array( 'default' => '' ) );

        // Sections
        $wp_customize->add_section(
            'contact-info',
            array(
                'title' => __( 'Contact Info', '_s' ),
                'priority' => 30,
                'description' => __( 'Contact info to appear on the footer.', '_s' )
            )
        );

        // Controls
        // Telephone
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'tel',
                array(
                    'label' => __( 'Telephone', '_s' ),
                    'section' => 'contact-info',
                    'settings' => 'tel'
                )
            )
        );
        // Email
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'email',
                array(
                    'label' => __( 'Email', '_s' ),
                    'section' => 'contact-info',
                    'settings' => 'email'
                )
            )
        );
        // Behance
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'behance',
                array(
                    'label' => __( 'Behance', '_s' ),
                    'section' => 'contact-info',
                    'settings' => 'behance'
                )
            )
        );
        // Linkedin
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'linkedin',
                array(
                    'label' => __( 'Linkedin', '_s' ),
                    'section' => 'contact-info',
                    'settings' => 'linkedin'
                )
            )
        );
        // Instagram
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'instagram',
                array(
                    'label' => __( 'Instagram', '_s' ),
                    'section' => 'contact-info',
                    'settings' => 'instagram'
                )
            )
        );
        
    }
    add_action('customize_register', 'contact_info');

?>