<?php

    // Work
    // -- Tags
    register_taxonomy(
        'work_tags', //taxonomy
        'al_work', //post-type
        array(
            'hierarchical'  => false,
            'label'         => __( 'Tags' ),
            'singular_name' => __( 'Tag' ),
            'rewrite'       => true,
            'query_var'     => true
        )
    );

    // -- Custom post type
    add_action( 'init', 'cpt_work' );
    function cpt_work() {
        $labels = array(
            'name'               => __( 'Work' ),
            'singular_name'      => __( 'Work' ),
            'menu_name'          => __( 'Work' ),
            'all_items'          => __( 'All work' ),
            'insert_into_item'   => __( 'Insert in work' ),
            'add_new'            => __( 'Add new work' ),
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'tags' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-images-alt2',
            'rewrite'            => array( 'slug' => 'work' ),
            'taxonomies'         => array( 'work_tags' ),
        );
        register_post_type( 'al_work', $args );
    };

?>