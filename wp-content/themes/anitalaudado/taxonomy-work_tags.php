<?php get_header(); ?>

    <?php
        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    ?>

    <!-- COVER -->
    <div class="page-cover tags-cover d-flex">
        <div class="container mt-auto">
            <div class="row">
                <div class="col-7 col-lg-7 d-lg-flex flex-column">
                    <h1 class="fs-30 mxy-0 mt-auto" data-aos="fade-up" data-aos-delay="400">#<?php echo $term->name; ?></h1>
                </div>

                <div class="col-5 col-lg-5 d-flex align-items-center justify-content-end">
                    <a class="primary-text fs-15 proj-link d-flex align-items-center reverse" href="<?php bloginfo('url'); ?>/work">
                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow-left.svg" alt="Anita Laudado">
                        All work
                    </a>
                </div>
            </div>
        </div>
    </div><!-- END COVER -->

    <?php
        if (have_posts()) : ?>
            <div id="site" class="container">
                <div class="row">
                    <?php while (have_posts()) :
                        the_post();
                        $description   = get_field( 'short_description');
                        $workThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

                        <div class="col-12 col-lg-4 mb-50">
                            <div class="work-col d-lg-flex flex-column">
                                <div onclick="location.href='<?php echo the_permalink(); ?>';" class="work-body">
                                    <div class="work-cover">
                                        <div class="hide lazyload w-100 h-100 work-img"
                                            style="background: url('<?php bloginfo('template_url'); ?>/dist/images/placeholder.png') center center/cover no-repeat"
                                            data-src="<?php echo $workThumbnail[0]; ?>">
                                        </div>
                                        <div class="show placeholder w-100 h-100 work-img"
                                            style="background: url('<?php bloginfo('template_url'); ?>/dist/images/placeholder.png') center center/cover no-repeat">
                                        </div>
                                    </div>

                                    <h1 class="primary-text fs-30 mxy-0"><?php the_title(); ?></h1>
                                    <p class="primary-text mxy-0"><?php echo $description ?></p>
                                </div>
                                <div class="work-footer mt-auto">
                                    <a class="primary-text fs-15 proj-link d-flex align-items-center" href="<?php echo the_permalink(); ?>">
                                        View more
                                        <img class="static" src="<?php bloginfo('template_url'); ?>/dist/images/arrow.svg" alt="Anita Laudado">
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif;
    ?>

<?php get_footer(); ?>